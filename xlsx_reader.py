import os
import sys

from zipfile import ZipFile
from libs import xlsx_sharedStrings as xss
from libs import xlsx_workbook as xwb
from libs import xlsx_sheet as xs

def _run(xlsx_file):
    sheets_data = []
    with ZipFile(xlsx_file, "r") as document:
        text_list = xss.get_sharedStrings(document.read("xl/sharedStrings.xml"))
        sheets_info = xwb.get_workbook(document.read("xl/workbook.xml"))
        for i in range(1, len(sheets_info)+1):
            sheets_data.append(xs.get_sheet(document.read(f"xl/worksheets/sheet{i}.xml"), text_list))

def main():
    try:
        xlsx_file = sys.argv[1]
        if not os.path.isfile(xlsx_file):
            raise IOError
    except Exception:
        sys.exit(1)

    _run(xlsx_file)

if __name__ == "__main__":
    main()