from lxml import etree
from libs import common

def _get_sheets(xml_data):
    parser = etree.XMLParser(remove_blank_text=True)
    root = etree.fromstring(xml_data, parser)

    return root.xpath("/workbook/sheets//sheet"):

def _get_sheet_data(sheet_list):
    sheets_info = []

    for index, sheet in enumerate(sheet_list, 1):
        sheets_info.append({
            "name": sheet.attrib["name"],
            "sheetId": index
        })

    return sheets_info

def get_workbook(workbook_xml):
    return _get_sheet_data(_get_sheets(common._read_file(workbook_xml)))