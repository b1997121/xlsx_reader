import re
import string
import math

from datetime import date, timedelta
from lxml import etree
from libs import common

def _get_row_data(xml_data):
    parser = etree.XMLParser(remove_blank_text=True)
    root = etree.fromstring(xml_data, parser)

    return root.xpath("/worksheet/sheetData//row")

def _alphabet_translate(target_str, al_list):
    value = int(math.pow(26, len(target_str)-1) * (al_list.index(target_str[0]) + 1))
    if len(target_str) == 1:
        return value
    else:
        return _alphabet_translate(target_str[1:], al_list) + value

def _get_sheet_text(rows_data, text_list):
    al_list = list(string.ascii_uppercase)
    row_list = []
    start_date = date(1900, 1, 1)
    row_num = 1
    for row in rows_data:
        row_i = int(row.attrib["r"])
        if row_i > row_num:
            for _ in range(row_i - row_num):
                row_list.append([""])
                row_num += 1
        row_num += 1
        col_list = []
        col_num = 1
        for col in row.xpath("c"):
            c_t = col.xpath("@t")[0] if col.xpath("@t") else ""
            c_s = col.xpath("@s")[0] if col.xpath("@s") else ""
            c_r = _alphabet_translate(re.findall("[A-Z]*", col.attrib["r"])[0], al_list)
            if c_r > col_num:
                for _ in range(c_r - col_num):
                    col_list.append("")
                    col_num += 1
            try:
                c_v = col.xpath("v")[0].text
            except (AttributeError, IndexError):
                c_v = ""

            if c_v:
                if c_t == "s":
                    c_v = text_list[int(c_v)]
                elif not c_t and c_s:
                    c_v = (timedelta(int(c_v) - 2) + start_date).strftime("%Y/%m/%d")

            col_list.append(c_v)
            col_num += 1
        row_list.append(col_list)
    return row_list

def get_sheet(sheet_xml, text_list):
    return _get_sheet_text(_get_row_data(common._read_file(sheet_xml)), text_list)
