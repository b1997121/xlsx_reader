import re
import os

from lxml import etree
from libs import common

def _get_si(xml_data):
    parser = etree.XMLParser(remove_blank_text=True)
    root = etree.fromstring(xml_data, parser)
    si_list = []

    for child in root:
        if child.tag == "si":
            si_list.append(child)

    return si_list

def _get_text(si_list):
    text_list = []
    for si in si_list:
        text = ""
        for t in si.xpath(".//t"):
            text += t.text if t.text else ""
        text_list.append(text)

    return text_list

def get_sharedStrings(sharedStrings_xml):
    return _get_text(_get_si(common._read_file(sharedStrings_xml)))
