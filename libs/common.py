import os
import re

def _read_file(xml):
    xml_data = xml
    if not isinstance(xml, str):
        xml_data = xml.decode("utf-8")
    if os.path.isfile(xml):
        with open(xml, "r") as xml_log:
            xml_data = xml_log.read()

    xml_data = re.sub("xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" ", "", xml_data)
    return xml_data.encode("utf-8")